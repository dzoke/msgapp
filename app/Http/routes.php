<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

global $app;

$app['router']->get('/', ['as' => 'index', 'uses' => 'IndexController@index']);

$app['router']->get('/messages', ['as' => 'messages', 'uses' => 'IndexController@allMessages']);

$app['router']->get('/logout', ['as' => 'logout', 'uses' => 'IndexController@logout']);

$app['router']->post('/login', ['as' => 'login', 'uses' => 'IndexController@login']);

$app['router']->get('/register', ['as' => 'register_get', 'uses' => 'IndexController@getRegister']);

$app['router']->post('/register', ['as' => 'register_post', 'uses' => 'IndexController@postRegister']);

$app['router']->get('/admin', ['as' => 'admin_index', 'uses' => 'AdminController@index']);

$app['router']->post('/admin/create_msg', ['as' => 'create_msg', 'uses' => 'AdminController@createMsg']);

$app['router']->get('/admin/deletemsg/{id}', ['as' => 'delete_msg', 'uses' => 'AdminController@deleteMsg'])->where('id', '[0-9]+');

// API

$app['router']->get('/api/messages/{format}', 'ApiController@allMessages');

$app['router']->get('/api/message/{id}/{format}', 'ApiController@getMessage')->where('id', '[0-9]+');

$app['router']->post('/api/create_msg', 'ApiController@createMsg');

$app['router']->post('/api/deletemsg', 'ApiController@deleteMsg');