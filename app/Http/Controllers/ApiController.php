<?php
namespace App\Http\Controllers;

use App\Message as Msg;
use App\User as User;
use Illuminate\Http\Request;
use Illuminate\Auth\AuthManager as Auth;
use Illuminate\Database\DatabaseManager as DB;
use Illuminate\Validation\Factory as Validator;
use Illuminate\Contracts\Mail\Mailer as Mail;
use Illuminate\Cache\Repository as CacheRepository;

class ApiController extends Controller
{

	public function __construct(
		Request $request,
		CacheRepository $cache,
		Auth $auth, Validator $validator,
		DB $db,
		Msg $msg,
		Mail $mail,
		User $user_model) {
		
		$this->auth = $auth;
		$this->request = $request;
		$this->db = $db;
		$this->validator = $validator;
		$this->msg = $msg;
		$this->mail = $mail;
		$this->allowedActions = array('getAllMessages', 'getMessage', 'deleteMessage', 'addMessage');
		$this->allowedFormats = array('json', 'xml');
		$this->user_model = $user_model;
		$this->cache = $cache;
	}

	public function allMessages($format)
	{
		
		if (empty($format) || !in_array($format, $this->allowedFormats)) {
			return json_encode(array('msg' => 'The format you have sent is not allowed. Choose json or xml.'));
		}

		$messages = $this->msg->leftJoin('users', 'users.id', '=', 'messages.user_id')
		                          ->select('users.name', 'messages.*')
		                          ->orderBy('messages.id','desc')
		                          ->get()->toArray();
		
		if ($format == 'json') {
			return json_encode($messages);
		}

		if ($format == 'xml') {
			$xml_data = new \SimpleXMLElement('<?xml version="1.0"?><data></data>');
			$this->arrayToXml($messages, $xml_data);
			$result = $xml_data->asXML();
			return $result;
		}
	}

	public function getMessage($id, $format)
	{
		if (empty($format) || !in_array($format, $this->allowedFormats)) {
			return json_encode(array('msg' => 'The format you have sent is not allowed. Choose json or xml.'));
		}

		if (empty($id)) {
			return array('msg' => 'ID is not set!');
		}

		$message = $this->msg->find($id)->toArray();

		if (!empty($message)) {
			if ($format == 'json') {
				return json_encode($message);
			}

			if ($format == 'xml') {
				$xml_data = new \SimpleXMLElement('<?xml version="1.0"?><data></data>');
				$this->arrayToXml($message,$xml_data);
				$result = $xml_data->asXML();
				return $result;
			}
		} else {
			return json_encode(array('msg' => 'Message not found!'));
		}
	}

	public function createMsg()
	{
		$validate = $this->validator->make(
		    array('msg_content' => $this->request->input('msg_content'),
		    	  'api_key' => $this->request->input('api_key')),
		    array('msg_content' => 'required|max:140|regex:/(^[A-Za-z0-9 ]+$)+/',
		    	  'api_key' => 'required|min:40|max:40|exists:users,api_key')
		);
		
		if ($validate->fails()) {
			return json_encode(array('msg' => $validate->messages()->first()));
		}
		
		$user = $this->user_model->whereApiKey($this->request->input('api_key'))->get()->first();

		if (!empty($user)) {
			try {
				$msgObj = new $this->msg;
				
				$msgObj->user_id = $user->id;
				$msgObj->msg_content = $this->request->input('msg_content');
				$msgObj->created_at = date('Y-m-d H:i:s');
				$msgObj->updated_at = date('Y-m-d H:i:s');
				
				$msgObj->save();

				if (config('myconfig.use_cache')) {
					if ($this->cache->has('messages')) {
						$this->cache->forget('messages');
					}

					//update all messages in cache
					$messages = $this->msg->leftJoin('users', 'users.id', '=', 'messages.user_id')
				                          ->select('users.name', 'messages.*')
				                          ->orderBy('messages.id','desc')
				                          ->get();

				    $this->cache->put('messages',$messages, 5);

				    //update user messages in cache

				    if ($this->cache->has('user_'.$user->id)) {
						$this->cache->forget('user_'.$user->id);
					}

				    $user_messages = $this->msg->where('user_id', $user->id)->orderBy('id','desc')->get();

				    $this->cache->forever('user_'.$user->id, $user_messages);
				}

				return json_encode(array('msg' => 'success'));

			} catch(Exception $e) {
				return json_encode(array('msg' => 'An error occured while trying to insert your msg. Please try again.'));
			}
		} else {
			return json_encode(array('msg' => 'user not found'));
		}
	}

	public function deleteMsg()
	{
		$validate = $this->validator->make(
		    array('id' => $this->request->input('id'),
		    	  'api_key' => $this->request->input('api_key')),
		    array('id' => 'required|integer|exists:messages,id',
		    	  'api_key' => 'required|min:40|max:40|exists:users,api_key')
		);
		
		if ($validate->fails()) {
			return json_encode(array('msg' => $validate->messages()->first()));
		}

		$message = $this->msg->find($this->request->input('id'));
		$user = $this->user_model->whereApiKey($this->request->input('api_key'))->get()->first();

		if ($user->id != $message->user_id) {
			return json_encode(array('msg' => 'You can\'t delete messages that are not created by you.'));
		}

		//check if there is a cache file with the user messages and use_cache config is set to true
		//if yes, get user messages from cache, otherwise get them from database
		if ($this->cache->has('messages') && config('myconfig.use_cache')) {
			$user_messages = $this->cache->get('messages');
		} else{
			$user_messages = $this->msg->where('user_id', $user->id)->orderBy('id','desc')->get();
		}

		if (!empty($message)) {
			try {
				$this->msg->where('id', $this->request->input('id'))->delete();
				
				if (config('myconfig.sync_mail')) {
					$this->mail->queue('email.msg_deleted',
						array('msg' => $message, 'user' => $user),
						function ($message) use ($user) {
							$message->from('msgapp@live.com', 'Message Web App');
							$message->to($user->email)->subject('Message deleted notification');
						}
					);
				} else {
					$this->mail->send('email.msg_deleted',
						array('msg' => $message, 'user' => $user),
						function ($message) use ($user) {
							$message->from('msgapp@live.com', 'Message Web App');
							$message->to($user->email)->subject('Message deleted notification');
						}
					);
				}

				//if use_cache config is set to true than update user_messages and messages file in cache
				if (config('myconfig.use_cache')) {

					if($this->cache->has('messages')) $this->cache->forget('messages');

					$messages = $this->msg->leftJoin('users', 'users.id', '=', 'messages.user_id')
			                          ->select('users.name', 'messages.*')
			                          ->orderBy('messages.id','desc')
			                          ->get();

					$this->cache->forever('messages', $messages);


					if ($this->cache->has('user_'.$user->id)) $this->cache->forget('user_'.$user->id);

					$user_messages = $this->msg->where('user_id', $user->id)->orderBy('id','desc')->get();

					$this->cache->forever('user_'.$user->id, $user_messages);
				}

				return json_encode(array('msg' => 'Message deleted successfully.'));

			} catch(Exception $e) {
				return json_encode(array('msg' => 'An error occured while trying to delete your msg. Please try again.'));
			}
		} else {
			return json_encode(array('msg' => 'Message with ID: '.$id.' was not found in database!'));
		}
	}

	function arrayToXml($data, &$xml_data)
	{
	    foreach ($data as $key => $value) {
	        if (is_array($value)) {
	            if (is_numeric($key)) {
	                $key = 'item'.$key;
	            }
	            $subnode = $xml_data->addChild($key);
	            $this->arrayToXml($value, $subnode);
	        } else {
	            $xml_data->addChild("$key", htmlspecialchars("$value"));
	        }
	     }
	}

}