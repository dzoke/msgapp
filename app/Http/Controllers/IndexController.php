<?php

namespace App\Http\Controllers;

use App\User;
use App\Message as Msg;
use Illuminate\Http\Request;
use Illuminate\View\Factory as View;
use Illuminate\Auth\AuthManager as Auth;
use Illuminate\Validation\Factory as Validator;
use Illuminate\Contracts\Hashing\Hasher as HasherContract;
use Illuminate\Database\DatabaseManager as DB;
use Illuminate\Cache\Repository as CacheRepository;

class IndexController extends Controller
{

	public function __construct(
		View $view,
		CacheRepository $cache, 
		Request $request, 
		Auth $auth,
		Validator $validator,
		HasherContract $hasher) {
		
        $this->view = $view;
        $this->cache = $cache;
        $this->request = $request;
        $this->auth = $auth;
        $this->validator = $validator;
        $this->hash = $hasher;
    }

    public function index()
    {
		if ($this->auth->check()) {
			return redirect('/admin');
		}

		$request = $this->request;

		return $this->view->make('login', compact('request'));
	}

	public function login()
	{
		if ($this->auth->check()) {
			return redirect('/admin');
		}

		$email = $this->request->input('email');
		$password = $this->request->input('password');

		$validate = $this->validator->make(
		    array('email' => $email,
		    	  'password' => $password
		    	  ),
		    array('email' => 'required|email|exists:users',
		    	  'password' => 'required|min:5'
		    	  )
		);
		
		if ($validate->fails()) {
			return redirect('/')->withInput()->withErrors($validate);
		}

		if ($this->auth->attempt(['email' => $email, 'password' => $password])) {
			return redirect('/admin');
		} else {
			return redirect('/')->withInput()->withErrors(['Erorr' => 'An error occured while trying to log you in.']);	
		}
	}

	public function getRegister()
	{
		if ($this->auth->check()) {
			return redirect('/admin');
		}

		$request = $this->request;

		return $this->view->make('register',compact('request'));
	}

	public function postRegister()
	{
		if ($this->auth->check()) {
			return redirect('/admin');
		}

		$name = $this->request->input('name');
		$email = $this->request->input('email');
		$password = $this->request->input('password');

		// set rules for validation
		$validate = $this->validator->make(
		    array('name' => $name,
		    	  'email' => $email,
		    	  'password' => $password
		    	  ),
		    array('name' => 'required',
		    	  'email' => 'required|email|unique:users',
		    	  'password' => 'required|min:5'
		    	  )
		);

		// validate input fields
		if ($validate->fails()) {
			$messages = $validate->messages();
			return redirect('/register')->withInput()->withErrors($validate);
		}

		//create new user
		$user = new User;

		$user->name = $name;
		$user->email = $email;
		$user->password = $this->hash->make($password);
		$user->api_key = $this->generateAppKey(40);
		$user->save();

		return redirect('/');
	}

	public function allMessages(Msg $msg)
	{
		//check if there is a cache file with the messages and use_cache config is set to true
		//if yes, get messages from cache, otherwise get them from database
		if ($this->cache->has('messages') && config('myconfig.use_cache')) {
			$messages = $this->cache->get('messages');
		} else {
			$messages = $msg->leftJoin('users', 'users.id', '=', 'messages.user_id')
		                          ->select('users.name', 'messages.*')
		                          ->orderBy('messages.id','desc')
		                          ->get();
		    $this->cache->put('messages', $messages, 5);
		}

		return $this->view->make('all_messages', compact('messages', $messages));
	}

	public function logout()
	{
		// clear user messages file from cache if exists
		if ($this->cache->has('user_'.$this->auth->user()->id && config('myconfig.use_cache'))) {
			$this->cache->forget('user_'.$this->auth->user()->id);
		}

		$this->auth->logout();
		return redirect('/');
	}

	function generateAppKey($length)
	{
		// generate random key
		$pool = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

    	$api_key = substr(str_shuffle(str_repeat($pool, $length)), 0, $length);

    	// check if the key already exists in the database
    	// if yes, generate another
	    if ($this->AppKeyExists($api_key)) {
	        return $this->generateAppKey();
	    }

	    return $api_key;
	}

	function AppKeyExists($api_key)
	{
	    return User::whereApiKey($api_key)->exists();
	}
}