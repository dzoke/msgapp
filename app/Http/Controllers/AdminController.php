<?php

namespace App\Http\Controllers;

use App\Message as Msg;
use App\User as User;
use Illuminate\Http\Request;
use Illuminate\View\Factory as View;
use Illuminate\Auth\AuthManager as Auth;
use Illuminate\Database\DatabaseManager as DB;
use Illuminate\Validation\Factory as Validator;
use Illuminate\Contracts\Mail\Mailer as Mail;
use Illuminate\Cache\Repository as CacheRepository;
use Illuminate\Session\Store as Session;

class AdminController extends Controller
{

	public function __construct(
		Request $request,View $view,
		Auth $auth,
		Validator $validator,
		Session $session,
		DB $db,
		Msg $msg,
		Mail $mail,
		CacheRepository $cache) {
		
		$this->auth = $auth;
		$this->session = $session;
		$this->view = $view;
		$this->request = $request;
		$this->db = $db;
		$this->validator = $validator;
		$this->msg = $msg;
		$this->mail = $mail;
		$this->cache = $cache;

		$this->beforeFilter(function(){
			if (!$this->auth->check()) {
				return redirect('/');
			}
		});
	}

	public function index()
	{
		$user = $this->auth->user();
		
		//check if there is a cache file with the user messages and use_cache config is set to true
		//if yes, get user messages from cache, otherwise get them from database
		if ($this->cache->has('user_'.$user->id) && config('myconfig.use_cache')) {
			$user_messages = $this->cache->get('user_'.$user->id);
		} else {
			$user_messages = $this->msg->where('user_id', $user->id)->orderBy('id','desc')->get();
			$this->cache->forever('user_'.$user->id, $user_messages);
		}

		$request = $this->request;
		$session = $this->session;
		return $this->view->make('admin.index', compact('user_messages', 'session', 'user', 'request'));
	}

	public function createMsg()
	{
		$user = $this->auth->user();

		$validate = $this->validator->make(
		    array('msg_content' => $this->request->input('msg_content')),
		    array('msg_content' => 'required|max:140|regex:/(^[A-Za-z0-9 ]+$)+/')
		);

		// validate input fields
		if ($validate->fails()) {
			return redirect('/admin')->withInput()->withErrors($validate);
		}
		
		try {
			//create new message
			$msgObj = new $this->msg;
			
			$msgObj->user_id = $user->id;
			$msgObj->msg_content = $this->request->input('msg_content');
			$msgObj->created_at = date('Y-m-d H:i:s');
			$msgObj->updated_at = date('Y-m-d H:i:s');
			
			$msgObj->save();

			//if use_cache config is set to true than update user_messages and messages file in cache
			if (config('myconfig.use_cache')) {

				if ($this->cache->has('messages')) {
					$this->cache->forget('messages');
				}
			
				//update all messages in cache
				$messages = $this->msg->leftJoin('users', 'users.id', '=', 'messages.user_id')
			                          ->select('users.name', 'messages.*')
			                          ->orderBy('messages.id','desc')
			                          ->get();

			    $this->cache->put('messages', $messages, 5);

			    //update user messages in cache

			    if ($this->cache->has('user_'.$user->id)) {
					$this->cache->forget('user_'.$user->id);
				}

			    $user_messages = $this->msg->where('user_id', $user->id)->orderBy('id','desc')->get();

			    $this->cache->forever('user_'.$user->id, $user_messages);
			}

			$this->session->flash('success', 'Your message was successfully created.');

			return redirect('/admin');

		} catch (Exception $e) {
			$this->session->flash('error', 'An error occured while trying to insert your msg. Please try again.');
			return redirect('/admin')->withInput();
		}
	}

	public function deleteMsg($id)
	{
		$message = $this->msg->find($id);
		$user = $this->auth->user();

		//check if there is a cache file with the user messages and use_cache config is set to true
		//if yes, get user messages from cache, otherwise get them from database
		if ($this->cache->has('messages') && config('myconfig.use_cache')) {
			$user_messages = $this->cache->get('messages');
		} else{
			$user_messages = $this->msg->where('user_id', $user->id)->orderBy('id','desc')->get();
		}

		if (!empty($message)) {
			try {
				$this->msg->where('id', $id)->delete();
				
				//if sync_mail is set to true, put email in queue
				// if not send email now
				if (config('myconfig.sync_mail')) {
					$this->mail->queue(
						'email.msg_deleted',
						array('msg' => $message, 'user' => $user),
						function ($message) use ($user) {
							$message->from('msgapp@live.com', 'Message Web App');
							$message->to($user->email)->subject('Message deleted notification');
						}
					);
				} else {
					$this->mail->send(
						'email.msg_deleted',
						array('msg' => $message, 'user' => $user),
						function ($message) use ($user) {
							$message->from('msgapp@live.com', 'Message Web App');
							$message->to($user->email)->subject('Message deleted notification');
						}
					);
				}

				//if use_cache config is set to true than update user_messages and messages file in cache
				if (config('myconfig.use_cache')) {

					if($this->cache->has('messages')) $this->cache->forget('messages');

					$messages = $this->msg->leftJoin('users', 'users.id', '=', 'messages.user_id')
			                          ->select('users.name', 'messages.*')
			                          ->orderBy('messages.id','desc')
			                          ->get();

					$this->cache->forever('messages', $messages);


					if ($this->cache->has('user_'.$user->id)) $this->cache->forget('user_'.$user->id);

					$user_messages = $this->msg->where('user_id', $user->id)->orderBy('id','desc')->get();

					$this->cache->forever('user_'.$user->id, $user_messages);
				}

				$this->session->flash('success', 'Message deleted successfully.');
				return redirect('/admin');

			} catch(Exception $e) {
				

				$msg = 'An error occured while trying to delete your msg. Please try again.';
				return $this->view->make('admin.index',compact('msg', 'user_messages' ));
			}
		} else {
			$this->session->flash('error', 'Message with ID: '.$id.' was not found in database!');
			return redirect('/admin');
		}
	}
}