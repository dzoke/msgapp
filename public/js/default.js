$('document').ready(function(){
	
	$('#msg_content').keyup(function(e){
		var msg_length = $(this).val().length;
		var char_left = 140 - msg_length;
		
		if(msg_length > 140){
			console.log(msg_length);
			e.preventDefault();
    		e.stopPropagation();
			return false;
		}	

		$('#msg_length').html(char_left);
	});

	$("#deletemsg").click(function(e){
		var c = confirm('Are you sure you want to delete this message?');

		if(c == false){
			e.preventDefault();
			return false;
		}
	});
});